package com.canang.course.domain;

/**
 * @author rafizan.baharum
 * @since 1/24/14
 */
public interface Application {

    String getFirstName();

    String getLastName();

    Course getCourse();

    Semester getSemester();
}
