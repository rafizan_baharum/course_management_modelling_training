package com.canang.course.domain;

/**
 * @author rafizan.baharum
 * @since 1/24/14
 */
public interface Campus {

    String getCode();

    String getName();
}
