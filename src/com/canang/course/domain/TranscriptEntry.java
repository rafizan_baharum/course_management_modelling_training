package com.canang.course.domain;

/**
 * @author rafizan.baharum
 * @since 1/24/14
 */
public interface TranscriptEntry extends MetaObject {

    Transcript getTranscript();

    Grade getGrade();

    Course getCourse();
}
