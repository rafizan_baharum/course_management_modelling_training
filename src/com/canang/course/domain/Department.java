package com.canang.course.domain;

/**
 * @author rafizan.baharum
 * @since 1/24/14
 */
public interface Department  extends MetaObject{

    String getCode();

    String getName();
}
