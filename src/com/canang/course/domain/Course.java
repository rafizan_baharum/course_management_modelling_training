package com.canang.course.domain;

import java.util.List;

/**
 * @author rafizan.baharum
 * @since 1/24/14
 */
public interface Course extends MetaObject {

    String getCourseNo();

    String getCourseName();

    Integer getCredits();

    Campus getCampus();

    List<Section> getSections();

    List<Prerequisite> getPrerequisites();
}
