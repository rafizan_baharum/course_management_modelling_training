package com.canang.course.domain;

/**
 * @author rafizan.baharum
 * @since 1/24/14
 */
public interface Semester {

    String getCode();

    Integer getYear();
}
