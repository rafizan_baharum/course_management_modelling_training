package com.canang.course.domain;

/**
 * @author rafizan.baharum
 * @since 1/24/14
 */
public interface Student extends Person {

    String getMatrixNo();

    String getMajor();

    String getDegree();

    Transcript getTranscript();
}
