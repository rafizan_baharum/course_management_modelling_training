package com.canang.course.domain;

import java.util.List;

/**
 * @author rafizan.baharum
 * @since 1/24/14
 */
public interface Section  extends MetaObject{

    String getSectionNo();

    String getDayOfWeek(); // M,T,W,Th,F,S,S

    String getTimeOfDay();

    String getRoom();

    String getCapacity();

    Semester getSemester();

    List<Enrollment> getEnrollments();

}
