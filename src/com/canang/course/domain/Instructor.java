package com.canang.course.domain;

import java.util.List;

/**
 * @author rafizan.baharum
 * @since 1/24/14
 */
public interface Instructor extends Person {

    Department getDepartment();

    List<Course> getCourse();
}
