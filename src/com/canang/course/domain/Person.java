package com.canang.course.domain;

/**
 * @author rafizan.baharum
 * @since 1/24/14
 */
public interface Person extends MetaObject{

    String getNricNo();
    String getFirstName();
    String getLastName();

}
